###

<!-- ![Build README](https://github.com/dqdang/dqdang/workflows/Build%20README/badge.svg) -->

<table><tr><td valign="top" width="25%">

### Message me
* [email](mailto:dqdang17@gmail.com)
* [messenger](https://www.m.me/dqdang1)
* [reddit](https://www.reddit.com/user/outsidefarmland)
* [PGP](https://raw.githubusercontent.com/dqdang/dqdang.github.io/master/derek-dang.asc)
* [coffee](https://www.buymeacoffee.com/dqdang)

</td><td valign="top" width="54%">

### Recent releases
<!-- recent_releases starts -->
* [dqdang Version 1.2](https://github.com/dqdang/dqdang/releases/tag/v1.2) - 2022-09-02
* [hold-my-liquor Version 1.4](https://github.com/dqdang/hold-my-liquor/releases/tag/v1.4) - 2022-04-04
* [ward-hop Version 0.2](https://github.com/dqdang/ward-hop/releases/tag/v0.2) - 2021-04-25
* [strafe Version 0.2](https://github.com/dqdang/strafe/releases/tag/v0.2) - 2021-04-25
* [digested-clips Version 2.3](https://github.com/dqdang/digested-clips/releases/tag/v2.3) - 2021-03-26
<!-- recent_releases ends -->

</td><td valign="top" width="21%">

### Fun stats
* [repo visualizer](http://ghv.artzub.com/#user=dqdang)
* [coder stats](https://coderstats.net/github/#dqdang)
* [oss insight](https://ossinsight.io/analyze/dqdang)
</td></tr></table>
